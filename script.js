"use strict"


const btns = document.querySelectorAll(".btn");


function clearBtnColor() {
    btns.forEach(btn => {
        btn.style.backgroundColor = "";
    });
}

document.addEventListener("keydown", function (event) {
    clearBtnColor();

    btns.forEach(btn => {
        const key = event.code.replace("Key", "").toUpperCase();

        if (btn.textContent.toUpperCase() === key) {
            btn.style.backgroundColor = "blue";
        }
    })
});

